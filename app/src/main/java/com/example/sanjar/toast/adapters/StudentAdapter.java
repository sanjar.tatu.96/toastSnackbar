package com.example.sanjar.toast.adapters;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sanjar.toast.MainActivity;
import com.example.sanjar.toast.R;
import com.example.sanjar.toast.database.DBHelper;
import com.example.sanjar.toast.models.StudentModel;

import java.util.ArrayList;

/**
 * Created by SANJAR on 13.02.2018.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder>{
    private ArrayList<StudentModel> data;
    public StudentAdapter(ArrayList<StudentModel> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student,parent,false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final StudentModel d=data.get(position);
        holder.firstName.setText(d.getFirstName());
        holder.lastName.setText(d.getLastName());
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popupMenu=new PopupMenu(holder.firstName.getContext(),view);
                popupMenu.inflate(R.menu.menu_edit_remove);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                       if (menuItem.getItemId()==R.id.remove){
                           final int adapterPosition=holder.getAdapterPosition();
                           final StudentModel d1=data.get(adapterPosition);
                           Snackbar snackbar=Snackbar.make(view,"Item deleted",Snackbar.LENGTH_LONG)
                                   .setAction("Undo", new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           data.add(adapterPosition,d1);
                                           DBHelper.getHelper(holder.firstName.getContext()).insertStudent(d1);
                                           notifyItemInserted(adapterPosition);
                                       }
                                   });
                            snackbar.setActionTextColor(Color.RED);
                            View snackbarView=snackbar.getView();
                            snackbarView.setBackgroundColor(Color.DKGRAY);
                            data.remove(adapterPosition);
                            DBHelper.getHelper(holder.firstName.getContext()).deleteStudent(d1);
                            notifyItemRemoved(adapterPosition);
                            snackbar.show();
                            return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView firstName;
        TextView lastName;
        ImageButton more;
        public ViewHolder(View itemView) {
            super(itemView);
            firstName=itemView.findViewById(R.id.first_name);
            lastName=itemView.findViewById(R.id.last_name);
            more=itemView.findViewById(R.id.more);
        }
    }
}
