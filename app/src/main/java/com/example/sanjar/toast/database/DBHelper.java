package com.example.sanjar.toast.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.example.sanjar.toast.models.StudentModel;

import java.util.ArrayList;

/**
 * Created by Sherzodbek on 22.12.2017. (Lesson19)
 */

public class DBHelper extends com.example.sanjar.toast.libs.DBHelper {
    @SuppressLint("StaticFieldLeak")
    private static DBHelper helper;

    public static DBHelper getHelper(Context context) {
        if (helper == null) {
            helper = new DBHelper(context);
        }
        return helper;
    }

    private DBHelper(Context context) {
        super(context, "student.db");
    }

    public ArrayList<StudentModel> getStudents() {
        ArrayList<StudentModel> students = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM students", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            students.add(new StudentModel(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("first_name")),
                    cursor.getString(cursor.getColumnIndex("last_name"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return students;
    }
    private ContentValues getStuentsData(StudentModel studentsModel) {
        ContentValues values = new ContentValues();
        values.put("first_name", studentsModel.getFirstName());
        values.put("last_name", studentsModel.getLastName());
        return values;
    }
    public void insertStudent(StudentModel studentsModel) {
        mDatabase.insert("students", null, getStuentsData(studentsModel));
    }
    public void updateStudent(StudentModel studentsModel) {
        mDatabase.update("students", getStuentsData(studentsModel), "id=" +studentsModel.getId(), null);
    }
    public void deleteStudent(StudentModel studentsModel){
        mDatabase.delete("students","id="+studentsModel.getId(),null);
    }
}
