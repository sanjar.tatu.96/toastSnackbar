package com.example.sanjar.toast;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.sanjar.toast.adapters.StudentAdapter;
import com.example.sanjar.toast.libs.DBHelper;
import com.example.sanjar.toast.models.StudentModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private StudentAdapter adapter;
    private ArrayList<StudentModel>studentModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.list);
        studentModels= com.example.sanjar.toast.database.DBHelper.getHelper(this).getStudents();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new StudentAdapter(studentModels);
        recyclerView.setAdapter(adapter);
    }
}
